# Amiga 2000 Slot to Socket

![Amiga 2000 Slot to Socket rendering](SlotToSock.png)

An adaptor to convert Amiga 2000 slot processors to 68K socket.

## Introduction

This board is designed to convert Amiga 2000 socket to slot adapators and PiStorm 2K boards to a 68K socket so that they can be tested in an Amiga 500 or ZZ9 PiStorm tester. Reducing wear & tear / risk to damage to the more expensive (in Europe) Amiga 2000.

![Amiga 2000 Slot to Socket usage](slottosock.jpg)

It only supports the regular 7MHz clock, so do not set the PiStorm 2K to use C1/C3 clocks prior to testing.

**Note:** This board was designed in KiCad 6.99 development release, it will not open in lower versions. It uses the KiBuzzard plugin with Topaz font for some of the text on the silk layer.

## BOM

| Symbol      | Component                                                |
| ----------- | -------------------------------------------------------- |
| C1, C2      | 0.1uF 0603                                               |
| J1          | 86 way 2.54mm pitch card edge connector (TE 1-5530843-0) |
| U1          | 2x32pin SILs machined pins 2.54mm                        |
